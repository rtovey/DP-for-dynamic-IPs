# Dynamical programming for off-the-grid dynamic Inverse Problems

This code accompanies the paper with the same name, https://arxiv.org/abs/2112.11378, enabling reproduction of the numerical examples therein. Documentation will also be added to enable the use of these algorithms more generally.

To reproduce all numerical results of this work:
- Create new conda environment: `conda env create -f DP_env.yml`
- Activate environment: `conda activate DP_env`
- run `python batch_recons.py`
- To recreate figures run the jupyter notebook `plots_general.ipynb`
- To see extra figures run the other two notebooks

The code in the directory `Bredies_code` is modified from the original which can be found at https://github.com/panchoop/DGCG_algorithm, which is supplementary to the paper by Bredies, Carioni, Fanzon, and Romero: https://arxiv.org/abs/2012.11706. The only modifications were to turn off printing and plotting functions. The new API is in `Bredies_wrapper.py`.

## Adapting the code for personal applications
The problem that this code solves is the optimisation problem:
$$ \min_\sigma \left\{ F(\sigma) + \int_\Gamma w(\gamma){\rm d}\sigma(\gamma) \quad|\quad \sigma\in\mathcal{M}(\Gamma)\right\}$$
where $\Gamma$ is either the space of balanced curves or curves with time-varying weight. See the paper for more detailed explanations of this notation.
The building blocks of the problem are the fidelity $F$ and the optimal transport cost $w$. A demo example is given in `demo.py` which simulates Fourier data and develops a custom transport cost for either the balanced or unbalanced cases. There are several examples of fidelity functions in `sparseDynamicRecon.fidelity.py`, including Fourier, Gaussian, windowed-Fourier, or with Airy disc point-spread functions. See documentation (e.g. `help(sparseDynamicRecon.fidelity.Fourier)`) for more information.