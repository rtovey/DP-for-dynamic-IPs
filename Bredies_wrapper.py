'''
Created on 5 Mar 2021

@author: Rob Tovey
'''
import time, numpy as np
from sparseDynamicRecon.utils import plotter, MovieMaker, callback
from sparseDynamicRecon.dynamicModelsBin import DynamicMeasureSpace, DynamicMeasure, CurveSpace
from sparseDynamicRecon.optimisation_algs import SLF
from Bredies_code.src import DGCG, config, classes, log_mod, insertion_step, optimization


def dynamic2Bredies(rho=None, FS=None):
    if hasattr(rho, 'FS'):
        rho, FS = rho.arr, rho.FS
    if FS is not None:
        if not isinstance(FS, BrediesMeasureSpace):
            FS = BrediesMeasureSpace(FS.FS, n=FS._maxindex)
    if rho is None:
        return FS
    elif FS is not None:
        return FS.element(rho), FS
    else:
        raise NotImplementedError


class BrediesMeasureSpace(DynamicMeasureSpace):
    def __init__(self, CS, n=0):
        super().__init__(CS, n)
        self._element = BrediesMeasure

    def from_curves(self, curves):
        n = len(curves.curves)  # number of curves
        T = len(self.T)  # number of time points
        rho = np.empty((n, 1 + 2 * T))
        rho[:, 0] = curves.weights / curves._energies
        if n + 1 > self._maxindex:
            self.extend(n + 1 - self._maxindex)
        for j in range(n):
            rho[j, 1:] = curves.curves[j].spatial_points.ravel()

        return self.element(rho)


class BrediesMeasure(DynamicMeasure):
    def __init__(self, arr, FS=None, this=None):
        super().__init__(arr, FS, this)

    @property
    def curves(self):
        T = self.T
        m = classes.measure()
        for row in self.arr:
            m.add(classes.curve(T, row[1:].reshape(-1, 2)), row[0])

        m.weights *= m._energies
        try:
            m.get_main_energy()
        except:
            pass
        return m

    @curves.setter
    def set_curves(self, curves):
        rho = self.FS.from_curves(curves)
        self.update(rho)


def BrediesFW(fidelity, OT, atoms=None, GT=None, mv=None, iters=100, doPlot=True, quiet=False,
              stop=0, **kwargs):
    if atoms is None:
        if GT is None:
            atoms = DynamicMeasureSpace(CurveSpace(dim=2, T=OT.T, balanced=True)).zero()
        else:
            atoms = DynamicMeasureSpace(GT.FS.FS).zero()
        atoms = dynamic2Bredies(FS=atoms.FS).zero()
    else:
        assert atoms.FS.balanced == OT.balanced == True
    pms = {'gap':-1, 'E':-1, 'E0':0, 'dof':0, 'i':1,
           'converged':0, 'ax1':(np.inf, -np.inf), 'ax2':(np.inf, -np.inf)}

    def cut_off(s):
        cutoff_threshold = 0.1
        transition = lambda s: 10 * s ** 3 - 15 * s ** 4 + 6 * s ** 5
        val = np.zeros(s.shape)
        for idx, s_i in enumerate(s):
            if cutoff_threshold <= s_i <= 1 - cutoff_threshold:
                val[idx] = 1
            elif 0 <= s_i < cutoff_threshold:
                val[idx] = transition(s_i / cutoff_threshold)
            elif 1 - cutoff_threshold < s_i <= 1:
                val[idx] = transition(((1 - s_i)) / cutoff_threshold)
        return val

    def D_cut_off(s):
        cutoff_threshold = 0.1
        D_transition = lambda s: 30 * s ** 2 - 60 * s ** 3 + 30 * s ** 4
        val = np.zeros(s.shape)
        for idx, s_i in enumerate(s):
            if 0 <= s_i < cutoff_threshold:
                val[idx] = D_transition(s_i / cutoff_threshold) / cutoff_threshold
            elif 1 - cutoff_threshold < s_i <= 1:
                val[idx] = -D_transition((1 - s_i) / cutoff_threshold) / cutoff_threshold
        return val

    def TEST_FUNC(t, x):
        input_fourier = x @ fidelity.locations[t].T
        fourier_evals = np.exp(-1j * input_fourier)
        cutoff = cut_off(x[:, 0:1]) * cut_off(x[:, 1:2])
        return fourier_evals * cutoff

    def GRAD_TEST_FUNC(t, x):
        freq_t = fidelity.locations[t].T
        input_fourier = x @ freq_t
        fourier_evals = np.exp(-1j * input_fourier)
        # cutoffs
        cutoff_1 = cut_off(x[:, 0:1])
        cutoff_2 = cut_off(x[:, 1:2])
        D_cutoff_1 = D_cut_off(x[:, 0:1])
        D_cutoff_2 = D_cut_off(x[:, 1:2])
        D_constant_1 = -1j * cutoff_1 @ freq_t[0:1] + D_cutoff_1
        D_constant_2 = -1j * cutoff_2 @ freq_t[1:2] + D_cutoff_2
        # wrap it up
        return np.concatenate([(fourier_evals * cutoff_2 * D_constant_1)[None],
                               (fourier_evals * cutoff_1 * D_constant_2)[None]], axis=0)

    DGCG.set_model_parameters(OT.weight[0], OT.weight[1], OT.T,
                              [f.shape[0] for f in fidelity.data],
                              TEST_FUNC, GRAD_TEST_FUNC)
    config.f_t = fidelity.data

    params = {
        "insertion_max_restarts": 1000,
        "insertion_min_restarts": 20,
        "results_folder": ".",
        "multistart_early_stop": lambda num_tries, num_found: np.inf,
        "multistart_pooling_num": 1000,
        "log_output": False,
        "insertion_max_segments": 5,
        'crossover':True,
        "TOL": 10 ** -10,
        "silent": True, "plot": False  # my additions
    }
    params.update(kwargs)
    if params['insertion_max_restarts'] < params['insertion_min_restarts']:
        raise Exception("insertion_max_restarts < insertion_min_restarts." +
                        "The execution is aborted")
    for k, v in params.items():
        setattr(config, k, v)
    if config.log_output:
        config.logger.logtext = ''
        config.logger.logcounter = 0
    config.insertion_eps = params['TOL']
    # Logger class, storing the used parameters
    config.logger = log_mod.logger()

    def energy(rho): return fidelity(rho) + OT(rho)

    maxtime = time.time() + 5 * 24 * 60 * 60  # impose maximum runtime of 5 days
    def extras(X=None, x=None, d=None, **kwargs):
        keys = 'gap', 'E', 'E0', 'dof', 'step'
        if X is None:
            return keys
        elif pms['E'] < 0:
            pms['E'] = energy(X)
            pms['E0'] = pms['E']
        pms['dof'] = X.arr.size
        out = [pms[k] for k in keys[:-1]] + [d]
        if -.5 < pms['gap'] < stop or time.time() > maxtime:
            pms['converged'] += 1
        pms['gap'] = -1
        return out

    doPlot, mv = plotter(GT, pms, mv, paths=True, doPlot=doPlot)

    def add_atom(rho):
        curves = rho.curves

        config.logger.status([1], pms['i'], curves)
        out, flag = insertion_step.insertion_step(curves)
        config.logger.status([2], pms['i'], curves)
        pms['flag'] = flag; pms['i'] += 1

        w_t = classes.dual_variable(curves)
        if len(out.curves) > 0:
            # Kristian uses: E(0)*( (c.dot(w_t))**2 - 1 )/2
            pms['gap'] = max(pms['gap'], max(c.integrate_against(w_t) - c.energy() for c in out.curves))
        return rho.FS.from_curves(out)

    def local_opt(rho):
        nextrho = rho.FS.from_curves(optimization.slide_and_optimize(rho.curves))
        pms.update(E=energy(nextrho))
        return nextrho

    cback = callback(iters=iters, frequency=(1.1 if iters / mv.fps > 5.1 else 1), quiet=quiet, record=('extras', 'stepsize'), extras=extras,
                     stop=lambda *_: pms['converged'] > 0, plot={'call':doPlot, 'fig':mv.fig})

    atoms = local_opt(atoms)  # initial local optimisation to improve initialisation
    atoms, cback = SLF(atoms, add_atom, local_opt, cback)
    mv.finish()
    return atoms, cback


if __name__ == '__main__':
    from sparseDynamicRecon import example
    np.random.seed(5)

    problem, RECORD, NOISE = 3, False, False
    fidelity, OT, GroundTruth = example(problem)

    iters = 20 if problem == 2 else 10

    for crossover in [True]:
        if NOISE:
            for p, params in enumerate(([1, .2], [1, .6], [3, .6])):
                fidelity, OT, GroundTruth = example(problem, noise=params[1])
                OT.scale_kernel(params[0])

                record = ('2D_Bredies%d_%d' % (problem, p) + ('' if crossover else '_nosplitting')) if RECORD else None
                mv = MovieMaker(filename=record, fps=4, dummy=record is None)
                recon, cback = BrediesFW(fidelity, OT, GT=GroundTruth,
                                               iters=iters, mv=mv,
                                               insertion_max_restarts=0,
                                               insertion_min_restarts=0,
                                               multistart_pooling_num=100,
        #                                        curves_list_length_lim=1,  # = nAtoms (I think...)
                                               slide_opt_max_iter=100000,
                                               multistart_early_stop=lambda num_tries, num_found: (np.inf if num_found == 0 else 0),  # I don't think this does anything...
                                               crossover=crossover,
                                               silent=True)
                print('\nFinal energies', OT(GroundTruth), cback.E[:, 1].min(), '\n')

        else:

            record = ('2D_Bredies%d' % problem + ('' if crossover else '_nosplitting')) if RECORD else None
            mv = MovieMaker(filename=record, fps=4, dummy=record is None)
            recon, cback = BrediesFW(fidelity, OT, GT=GroundTruth,
                                           iters=iters, mv=mv,
                                           insertion_max_restarts=0,
                                           insertion_min_restarts=0,
                                           multistart_pooling_num=100,
    #                                        curves_list_length_lim=1,  # = nAtoms (I think...)
                                           slide_opt_max_iter=100000,
                                           multistart_early_stop=lambda num_tries, num_found: (np.inf if num_found == 0 else 0),  # I don't think this does anything...
                                           crossover=crossover,
                                           silent=True)

            print('\nFinal energies', OT(GroundTruth), cback.E[:, 1].min(), '\n')

    # from matplotlib import pyplot as plt; plt.show()
