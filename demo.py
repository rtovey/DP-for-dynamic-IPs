'''
Created on 16 Feb 2022

@author: Rob Tovey
'''
import numpy as np
from matplotlib import pyplot as plt
from sparseDynamicRecon.fidelity import Fourier
from sparseDynamicRecon import (DynamicMeasureSpace, CurveSpace, pathOT, UBpathOT,
                                save, load, MovieMaker, randomDynamicFW, uniformDynamicFW)

dim = 2  # code is only currently capable of processing 2D reconstructions
balanced = True  # whether the masses should be balanced or un-balanced
T = 21  # number of time-points
curves = 2  # number of curves in ground-truth (optional, only needed for simulation)
samples = 10  # number of samples in data for each time-point
noise = 0  # noise to add on to synthetic signal

space = DynamicMeasureSpace(
            CurveSpace(dim=dim, T=np.linspace(0, 1, T), balanced=balanced),
            n=curves
        )

GT = space.zero()  # initialise a null ground-truth

# Initialise locations of two curves:
GT[0].x[:] = [[.2 + .6 * t, .2 + .6 * t] for t in space.T]
GT[1].x[:] = [[.8 - .6 * t, .2 + .6 * t] for t in space.T]

if balanced:  # all unit mass for all times
    GT.a[:] = 1
else:  # different masses at each time
    GT[0].a[:] = [.5 * (1 + 3 * t ** 2) for t in space.T]
    GT[1].a[:] = [1.5 * (1 - t) ** .5 for t in space.T]

# Assume data is given by convolution with Fourier kernels with given frequencies.
t = np.linspace(0, 2 * np.pi, samples)[:, None]  # data sampling time
frequencies = .2 * t * np.concatenate((np.cos(t), np.sin(t)), axis=1)
F = Fourier(2 * np.pi * frequencies, data=np.zeros((T, samples), dtype='complex128'))
GT_data = F.fwrd(GT)  # use simulated data in this example
# Create normalised random noise:
noise_vec = np.random.normal(0, 1, GT_data.shape)
noise_vec *= noise * np.linalg.norm(GT_data.ravel()) / np.linalg.norm(noise_vec.ravel())
F.data = GT_data + noise_vec

from numba import jit, prange
if balanced:
    class myOT(pathOT):
        '''
        Represents a transport cost computed as a penalty on curves
        In this example we have the Benamou-Brenier penalty:
            OT(sigma) = sum_i ( alpha + sum_t beta(t)*(x_i(t+1)-x(t))^2 )*sigma_i
        where sigma is a sum of curves with trajectories x_i(t) and constant mass sigma_i.
            
        '''
        def update_kernel(self, **kwargs):
            # This function is called whenever the OT parameters are modified
            self._ker_params = {k:kwargs.get(k, v) for k, v in self._ker_params.items()}
            # sigma is stored as an array of the form:
            #    sigma[i,:] = [sigma_i, x(0), y(0), ..., x(1), y(1)]
            self._shape = -1, 1 + 2 * self.T.size

            # Setting a maximum velocity for particles can reduce computation time:
            self.velocity = self._ker_params['velocity']

            dt = 1 / (T - 1)
            W = self._ker_params['weight']
            alpha, beta = W[0], W[1] / (2 * dt)

            @jit('void(f8[:,:],f8[:])', parallel=True, nopython=True)
            def energy(sigma, out):
                # computes out[i] = ( alpha + sum_t beta(t)*(x_i(t+1)-x_i(t))^2 )*sigma_i
                for i in prange(sigma.shape[0]):  # for each curve in sigma
                    s = alpha
                    for j in range(T - 1):
                        s += beta * (abs(sigma[i, 1 + 2 * j] - sigma[i, 3 + 2 * j]) ** 2
                                         +abs(sigma[i, 2 + 2 * j] - sigma[i, 4 + 2 * j]) ** 2)
                    out[i] = sigma[i, 0] * s

            @jit('void(f8[:,:],f8[:],f8[:,:])', parallel=True, nopython=True)
            def grad(sigma, out, outgrad):
                # computes out[i] = ( alpha + beta*sum_t (x_i(t+1)-x(t))^2 )*sigma_i
                # and outgrad[i,j] = d/d(sigma[i,j]) out[i]
                for i in prange(sigma.shape[0]):
                    s = alpha
                    K = 0
                    for j in range(T - 1):
                        for k in range(2):
                            tmp = sigma[i, 1 + K] - sigma[i, 3 + K]
                            s += beta * tmp ** 2  # add to energy

                            tmp *= 2 * beta  # rescale for gradient
                            outgrad[i, K] += tmp
                            outgrad[i, 2 + K] -= tmp

                            K += 1  # K = 2*j + k
                    out[i] = s

            @jit('void(f8[:,:],f8[:,:],f8,f8[:,:])', parallel=True, nopython=True)
            def step_mesh(x, X, dt, out):
                # computes cost of moving a unit mass from x[i] to X[j] in time dt
                # i.e. out[i,j] = alpha * dt + beta*(x[i]-X[j])^2
                alpha = W[0] * dt
                beta = W[1] / (2 * dt)
                for i in prange(x.shape[0]):
                    for j in range(X.shape[0]):
                        out[i, j] = alpha + beta * (
                            abs(x[i, 0] - X[j, 0]) ** 2 + abs(x[i, 1] - X[j, 1]) ** 2)

            self.kernel = (energy, grad, step_mesh)

else:
    class myOT(UBpathOT):
        '''
        Represents an unbalanced transport cost computed as a penalty on curves
        In this example we have a simple quadratic penalty motivated by the WFR cost:
            OT(sigma) = sum_i sum_t ( alpha/2*(h_i(t)+h_i(t+1)) + beta*(x_i(t+1)-x_i(t))^2 
                            + gamma*(sqrt(h_i(t+1))-sqrt(h_i(t)))^2)
        where sigma is a sum of curves with trajectories x_i(t) with mass h_i(t).
        
        The biggest difference from the balanced case is that the physical energy scales linearly 
        with h, but is not smooth enough to have a gradient in h. Because of this we implement a
        change of basis (methods: toarray, fromarray) to go between the smooth and the 'original' 
        parametrisations. The energy and grad functions are performed on the smooth parametrisation, 
        but the step_mesh function is performed on the original parametrisation.
        It is assumed that the fidelity acts on the original parametrisation of the measure. 
            
        '''
        def toarray(self, rho, grad=False):
            # mapping from linear, to smooth representations. In this case, squaring the mass.
            # if grad=True then also return the derivative of the change of basis
            rho = self._parse_rho(rho)  # rho is a measure with linear mass
            M = rho.copy()
            M[:,::3] *= M[:,::3]  # square the mass
            if grad:
                dM = np.ones(rho.shape)
                dM[:,::3] = 2 * rho[:,::3]
                return M, dM
            else:
                return M

        def fromarray(self, M):
            # The inverse of self.toarray, mapping back to the original parametrisation
            rho = M.copy()
            rho[:,::3] = np.sqrt(rho[:,::3])
            return rho

        def update_kernel(self, **kwargs):
            # This function is called whenever the OT parameters are modified
            self._ker_params = {k:kwargs.get(k, v) for k, v in self._ker_params.items()}
            # sigma is stored as an array of the form:
            #    sigma[i,:] = [h(0), x(0), y(0), ..., h(1), x(1), y(1)]
            self._shape = -1, 3 * self.T.size

            # Setting a maximum velocity for particles can reduce computation time:
            self.velocity = self._ker_params['velocity']

            dt = 1 / (T - 1)
            W = self._ker_params['weight']
            alpha, beta, gamma = W[0] * .5 * dt, W[1] / (2 * dt), W[2] / (2 * dt)

            @jit('void(f8[:,:],f8[:])', parallel=True, nopython=True)
            def energy(sigma, out):
                for i in prange(sigma.shape[0]):  # for each curve in sigma
                    s = 0
                    for j in range(T - 1):
                        J = 3 * j
                        h, x, y = sigma[i, J], sigma[i, J + 1], sigma[i, J + 2]
                        H, X, Y = sigma[i, J + 3], sigma[i, J + 4], sigma[i, J + 5]

                        s += alpha * (h ** 2 + H ** 2)
                        s += beta * h * H * ((x - X) ** 2 + (y - Y) ** 2)
                        s += gamma * (h - H) ** 2
                    out[i] = s

            @jit('void(f8[:,:],f8[:],f8[:,:])', parallel=True, nopython=True)
            def grad(sigma, out, outgrad):
                for i in prange(sigma.shape[0]):
                    s = 0
                    for j in range(T - 1):
                        J = 3 * j
                        h, x, y = sigma[i, J], sigma[i, J + 1], sigma[i, J + 2]
                        H, X, Y = sigma[i, J + 3], sigma[i, J + 4], sigma[i, J + 5]
                        d = (x - X) ** 2 + (y - Y) ** 2

                        s += alpha * (h ** 2 + H ** 2)
                        s += beta * h * H * d
                        s += gamma * (h - H) ** 2

                        outgrad[i, J + 0] += 2 * alpha * h + beta * H * d + 2 * gamma * (h - H)
                        outgrad[i, J + 3] += 2 * alpha * H + beta * h * d + 2 * gamma * (H - h)

                        outgrad[i, J + 1] += 2 * beta * h * H * (x - X)
                        outgrad[i, J + 2] += 2 * beta * h * H * (y - Y)
                        outgrad[i, J + 4] += 2 * beta * h * H * (X - x)
                        outgrad[i, J + 5] += 2 * beta * h * H * (Y - y)
                    out[i] = s

            @jit('void(f8[:,:],f8[:],f8[:,:],f8[:],f8,f8[:,:,:,:])', parallel=True, nopython=True)
            def step_mesh(x, h, X, H, dt, out):
                alpha = W[0] * .5 * dt
                beta = W[1] / (2 * dt)
                gamma = W[2] / (2 * dt)
                for i0 in prange(x.shape[0]):
                    xx, yy = x[i0, 0], x[i0, 1]
                    for i1 in prange(h.shape[0]):
                        hh = np.sqrt(h[i1])
                        for j0 in range(X.shape[0]):
                            XX, YY = X[j0, 0], X[j0, 1]
                            for j1 in range(H.shape[0]):
                                HH = np.sqrt(H[j1])
                                out[i0, i1, j0, j1] = (alpha * (hh**2 + HH**2)
                                                    +beta * hh * HH * ((xx - XX) ** 2 + (yy - YY) ** 2)
                                                    +gamma * (hh - HH) ** 2
                                                    )

            self.kernel = (energy, grad, step_mesh)

# The velocity parameter is active when the uniform grid FW algorithm is used.
# No paths will be searched for with maximum velocity greater than this value.
# If the time inteval is [0,1], then this should be the maximum number of times a
# particle could travel from left-to-right accross the domain in a unit time.
# Alternatively, velocity/T is the fraction of the image that can be crossed in
# one frame.
OT = myOT(weight=(1, 1) if balanced else (1, 1, 1), velocity=10, T=space.T)
OT.scale_kernel(.1)  # rescale all weights by 0.1

recon_rand, cback_rand = randomDynamicFW(F, OT, GT=GT,
                            constraint=10,  # the mass of each curve is 1<<10
                            iters=10,  # number of iterations
                            nAtoms=1,  # number of new atoms to add at every iteration
                            meshsize=25 ** 2,  # number of random points to sample at each time
                            masses=1 if balanced else 10, # number of random masses to consider at each time
                            # instructions for saving reconstruction video, ommit if not required
                            mv=MovieMaker(filename=('demo_results', 'vid_rand'), fps=4),
                        )
save(('demo_results', 'recon_rand'), DM=recon_rand, CB=cback_rand)
recon_rand, cback_rand = load(('demo_results', 'recon_rand'))

recon_uniform, cback_uniform = uniformDynamicFW(F, OT, GT=GT,
                            constraint=10,  # the mass of each curve is 1<<10
                            iters=10,  # number of iterations
                            nAtoms=1,  # number of new atoms to add at every iteration
                            levels=3,  # start with a 8x8 (i.e. 8 = 2^3)
                            maxlevel=6,  # maximum grid-size is 64x64
                            masses=1 if balanced else 10, # number of random masses to consider at each time
                            mv=MovieMaker(filename=('demo_results', 'vid_uniform'), fps=4),
                        )
save(('demo_results', 'recon_uniform'), DM=recon_uniform, CB=cback_uniform)
recon_uniform, cback_uniform = load(('demo_results', 'recon_uniform'))

# Plot results:
plt.close()  # close figure for videos
plt.figure('Recon comparison', figsize=(18, 10))
try:  # try to move figure to top-left corner of screen
    window = plt.gcf().canvas.manager.window
    if hasattr(window, 'move'):
        window.move(0, 0)
    elif hasattr(window, 'SetPosition'):
        window.SetPosition((0, 0))
    elif hasattr(window, 'wm_geometry'):
        window.wm_geometry('+0+0')
except:
    pass

# Plot reconstruction with random grids
ax = plt.subplot(221)
GT.plot(ax=ax, alpha=.4, width=40)  # Ground-truth plotted as faint lines
recon_rand.plot(ax=ax, alpha=.8, width=40)  # Reconstruction plotted as strong lines
ax.set_title('Random grid reconstruction'); ax.set_aspect('equal')
ax.set_xlim(0, 1); ax.set_ylim(0, 1)
ax.set_xlabel('x'); ax.set_ylabel('y')

# Plot reconstruction with uniform grids
ax = plt.subplot(222)
GT.plot(ax=ax, alpha=.4, width=40)  # Ground-truth plotted as faint lines
recon_uniform.plot(ax=ax, alpha=.8, width=40)  # Reconstruction plotted as strong lines
ax.set_title('Uniform grid reconstruction'); ax.set_aspect('equal')
ax.set_xlim(0, 1); ax.set_ylim(0, 1)
ax.set_xlabel('x'); ax.set_ylabel('y')

# Prelims for plotting convergence of energy
keys = list(cback_rand.extras())  # the list of values saved during reconstruction
EMin = min(cback_rand.E[:, keys.index('E')].min(), cback_uniform.E[:, keys.index('E')].min())
EMax = min(cback_rand.E[:, keys.index('E')].max(), cback_uniform.E[:, keys.index('E')].max())

print(cback_rand.E[1:, keys.index('E')] - cback_rand.E[:-1, keys.index('E')])
print(cback_uniform.E[1:, keys.index('E')] - cback_uniform.E[:-1, keys.index('E')])

# Plot convergence of energy (shifted to the range [0,1]) and step-size |x_n - x_{n-1}|
# Note that the step-size is a naive array norm, it doesn't track deletion/reordering of curves
ax = plt.subplot(223)
ax.plot(cback_rand.i, (cback_rand.E[:, keys.index('E')] - EMin) / (EMax - EMin), label='Energy, random grid')
ax.plot(cback_uniform.i, (cback_uniform.E[:, keys.index('E')] - EMin) / (EMax - EMin), label='Energy, uniform grid')
ax.plot(cback_rand.i, cback_rand.E[:, keys.index('step')], label='Change, random grid')
ax.plot(cback_uniform.i, cback_uniform.E[:, keys.index('step')], label='Change, uniform grid')
ax.set_title('Convergence with respect to iteration number'); ax.set_xlabel('iteration')
plt.legend(loc='upper right')

# Plot convergence of energy and step-size with respect to time
ax = plt.subplot(224)
ax.plot(cback_rand.T, (cback_rand.E[:, keys.index('E')] - EMin) / (EMax - EMin), label='Energy, random grid')
ax.plot(cback_uniform.T, (cback_uniform.E[:, keys.index('E')] - EMin) / (EMax - EMin), label='Energy, uniform grid')
ax.plot(cback_rand.T, cback_rand.E[:, keys.index('step')], label='Change, random grid')
ax.plot(cback_uniform.T, cback_uniform.E[:, keys.index('step')], label='Change, uniform grid')
ax.set_title('Convergence with respect to time'); ax.set_xlabel('time (s)')
plt.legend(loc='upper right')

plt.tight_layout();plt.show()
