'''
Created on 20 Apr 2021

@author: Rob Tovey
'''
from sparseDynamicRecon import MovieMaker, save, load, example, randomDynamicFW, uniformDynamicFW
import numpy as np
from tqdm import trange
from Bredies_wrapper import BrediesFW

# these values are used in the comparable study by Bredies et al.
from sparseDynamicRecon import utils
utils.ZERO, utils.CURVE_TOL = 1e-8, 1e-5


def BrediesRecon(F, OT, GT, problem, filename, p, LOAD=True):
    if type(p) is int:
        crossover, = BrediesParams[p]
        record = filename + ('Bredies' + ('' if crossover else '_nosplitting'),)
        kwargs = BrediesExact[problem]
    else:
        crossover, = BrediesParams[p[0]]
        record = filename + ('Bredies' + ('' if crossover else '_nosplitting')
                             +('_accel' if p[-1] == 'accelerated' else ''),)
        kwargs = BrediesExact[(problem,) + p[1:]]

    iters = 21 if problem == 2 else 10

    # default arguments
    my_kwargs = dict(insertion_max_restarts=0, insertion_min_restarts=15,
                   multistart_pooling_num=1000, slide_opt_max_iter=100000,
                   crossover=crossover, silent=True)
    my_kwargs.update(kwargs)

    if my_kwargs.get('multistart_early_stop', None) is None:
        my_kwargs.pop('multistart_early_stop', None)
    elif my_kwargs['multistart_early_stop'] == 'accelerated':
        # Early stop function
        def early_stop(num_tries, num_found):
            def expected_num_found(num_tries, total_num):
                return total_num * (1 - ((total_num - 1) / total_num) ** num_tries)

            def expected_total_num(num_tries, num_found):
                above_total_num = 10
                while expected_num_found(num_tries, above_total_num) < num_found:
                    above_total_num = 10 * above_total_num
                below_total_num = above_total_num / 10
                # expected_num_found(below_total_num) < num_found < exp...(above)
                while above_total_num - below_total_num > 1:
                    # binomial search
                    mid_point = (below_total_num + above_total_num) / 2
                    if expected_num_found(num_tries, mid_point) == num_found:
                        return mid_point
                    elif expected_num_found(num_tries, mid_point) < num_found:
                        below_total_num = mid_point
                    else:
                        above_total_num = mid_point
                return above_total_num

            total_num = expected_total_num(num_tries, num_found)

            fail_probability = 0.01
            suggested = np.log(fail_probability) / np.log((total_num - 1) / total_num)
            return np.ceil(suggested)

        my_kwargs['multistart_early_stop'] = early_stop

    if LOAD:
        try:
            return load(record)
        except FileNotFoundError:
            pass
    np.random.seed(123)
    mv = MovieMaker(filename=record, fps=4)
    recon, cback = BrediesFW(F, OT, GT=GT, stop=1e-16, iters=iters, mv=mv, doPlot=True, **my_kwargs)
    save(record, DM=recon, CB=cback)

    return recon, cback


def randomRecon(F, OT, GT, problem, filename, p, LOAD=True):
    nAtoms, meshsize = curveParams[p]

    record = filename + ('randpaths_%d_%d' % (nAtoms, meshsize),)
    iters = 10000 if problem == 2 else 100  # both of these are excessive

    if LOAD:
        try:
            return load(record)
        except FileNotFoundError:
            pass
    np.random.seed(456)
    mv = MovieMaker(filename=record, fps=4)
    recon, cback = randomDynamicFW(F, OT, constraint=10, GT=GT, iters=iters, mv=mv,
                                   nAtoms=nAtoms, meshsize=meshsize ** 2)
    save(record, DM=recon, CB=cback)

    return recon, cback


def randomUBRecon(F, OT, GT, problem, filename, p, LOAD=True):
    nAtoms, meshsize, masses = curveParamsUB[p]

    record = filename + ('randpathsUB_%d_%d_%d' % (nAtoms, meshsize, masses),)
    iters = 1000 if problem == 2 else 100  # both of these are excessive

    if LOAD:
        try:
            return load(record)
        except FileNotFoundError:
            pass
    np.random.seed(456)
    mv = MovieMaker(filename=record, fps=4)
    recon, cback = randomDynamicFW(F, OT, constraint=10, GT=GT, iters=iters, mv=mv,
                                   nAtoms=nAtoms, masses=masses, meshsize=meshsize ** 2)
    save(record, DM=recon, CB=cback)

    return recon, cback


def randomReconReps(F, OT, GT, reps, problem, filename, p, LOAD=True):
    nAtoms, meshsize = curveParams[p]

    iters = 1000 if problem == 2 else 10

    for i in trange(reps):
        np.random.seed(12 + 34 * i)
        record = filename + ('randpaths_%d_%d_%d' % (nAtoms, meshsize, i),)
        if LOAD:
            try:
                load(record)
                continue
            except FileNotFoundError:
                pass
        recon, cback = randomDynamicFW(F, OT, constraint=10, GT=GT, nAtoms=nAtoms, iters=iters,
                                       meshsize=meshsize ** 2, doPlot=False, quiet=True)
        save(record, DM=recon, CB=cback)


def randomUBReconReps(F, OT, GT, reps, problem, filename, p, LOAD=True):
    nAtoms, meshsize, masses = curveParamsUB[p]

    iters = 100 if problem == 2 else 10

    for i in trange(reps):
        np.random.seed(12 + 34 * i)
        record = filename + ('randpathsUB_%d_%d_%d_%d' % (nAtoms, meshsize, masses, i),)
        if LOAD:
            try:
                load(record)
                continue
            except FileNotFoundError:
                pass
        recon, cback = randomDynamicFW(F, OT, constraint=10, GT=GT, nAtoms=nAtoms, iters=iters,
                                       masses=masses, meshsize=meshsize ** 2, doPlot=False, quiet=True)
        save(record, DM=recon, CB=cback)


def UniformRecon(F, OT, GT, problem, filename, p, LOAD=True):
    nAtoms, maxlevel, gpu = UniformParams[p]

    record = filename + ('uniformpaths_%d_%d' % (nAtoms, maxlevel) + ('_gpu' if gpu else ''),)
    iters = 1000

    if LOAD:
        try:
            return load(record)
        except FileNotFoundError:
            pass

    # edit GPU preference
    from sparseDynamicRecon import DynamicPathSelect
    oldgpu = DynamicPathSelect.GPU; DynamicPathSelect.GPU = gpu

    mv = MovieMaker(filename=record, fps=4)
    recon, cback = uniformDynamicFW(F, OT, constraint=10, GT=GT, iters=iters, mv=mv,
                                   nAtoms=nAtoms, levels=4, maxlevel=maxlevel)

    DynamicPathSelect.GPU = oldgpu  # revert to original default GPU settings

    save(record, DM=recon, CB=cback)

    return recon, cback


def UniformUBRecon(F, OT, GT, problem, filename, p, LOAD=True):
    nAtoms, maxlevel, masses, gpu = UniformParamsUB[p]

    record = filename + ('uniformpathsUB_%d_%d_%d' % (nAtoms, maxlevel, masses) + ('_gpu' if gpu else ''),)
    iters = 100

    if LOAD:
        try:
            return load(record)
        except FileNotFoundError:
            pass

    # edit GPU preference
    from sparseDynamicRecon import DynamicPathSelect
    oldgpu = DynamicPathSelect.GPU; DynamicPathSelect.GPU = gpu

    mv = MovieMaker(filename=record, fps=4)
    recon, cback = uniformDynamicFW(F, OT, constraint=10, GT=GT, iters=iters, mv=mv,
                                    nAtoms=nAtoms, levels=4, maxlevel=maxlevel, masses=masses)

    DynamicPathSelect.GPU = oldgpu  # revert to original default GPU settings
    save(record, DM=recon, CB=cback)
    return recon, cback


BrediesParams = ([True], [False])  # crossover or not
BrediesDict = {'all':range(2), 'multiple':range(2), 'single':(0,)}
BrediesExact = {
    1:{'insertion_max_restarts': 20, 'multistart_pooling_num': 100},
    (2, 0):{'insertion_max_restarts': 200, 'multistart_pooling_num': 1000},
    (2, .2):{'insertion_max_restarts': 1000, 'multistart_pooling_num': 1000},
    (2, .6):{'insertion_max_restarts': 10000, 'multistart_pooling_num': 1000},
    (2, .6, 'reg'):{'insertion_max_restarts': 5000, 'multistart_pooling_num': 1000},
    (2, .6, 'accelerated'):{'insertion_max_restarts': 10000, 'insertion_min_restarts': 5000,
                            'multistart_pooling_num': 1000, 'multistart_early_stop':'accelerated'},
    3:{'insertion_max_restarts': 50, 'insertion_min_restarts': 20, 'multistart_pooling_num': 100},
}
BrediesExact[2] = BrediesExact[(2, 0)]
# These examples are just too trivial to use multiple restarts:
BrediesExact[1] = BrediesExact[3] = {'insertion_max_restarts': 0, 'insertion_min_restarts': 0, 'multistart_pooling_num': 100}

curveParams = tuple([i, j] for i in (1, 5, 25) for j in (5, 10, 25))
curveDict = {'all':range(9), 'single':(curveParams.index([1, 25]),),
             'multiple':tuple(i for i, j in enumerate(curveParams) if j[0] == 1)}
UniformParams = tuple([i, j, False] for i in (1, 5, 25) for j in (8,)) + tuple([i, 8, True] for i in (1, 5, 25))
UniformDict = {'all':range(len(UniformParams)), 'single':(UniformParams.index([1, 8, False]),)}

curveParamsUB = tuple([i, j, k] for i in (1, 5, 25) for j in (5, 10, 25) for k in (0, 5, 10))
curveDictUB = {'all':range(len(curveParamsUB)), 'single':(curveParamsUB.index([1, 25, 10]),),
             'multiple':tuple(i for i, j in enumerate(curveParamsUB) if j[0] == 1 and j[2] == 10)}
UniformParamsUB = tuple([i, j, k, True] for i in (1, 5, 25) for j in (7,) for k in (0, 5, 10))
UniformDictUB = {'all':range(len(UniformParamsUB)), 'single':(UniformParamsUB.index([1, 7, 10, True]),)}

alg_configs = {
    'Bredies': (BrediesRecon, BrediesDict),
    'Random': (randomRecon, curveDict),
    'RandomUB': (randomUBRecon, curveDictUB),
    'RandomRep': (randomReconReps, curveDict),
    'RandomUBRep': (randomUBReconReps, curveDictUB),
    'Uniform': (UniformRecon, UniformDict),
    'UniformUB': (UniformUBRecon, UniformDictUB),
}

noise_configs = ([[1] * 2, 0], [[1] * 2, .2], [[1] * 2, .6], [[3] * 2, .6])
scale_configs = ([[.3, .3], 0], [[.3, 1.], 0], [[.3, 3.], 0],
                 [[1., .3], 0], [[1., 1.], 0], [[1., 3.], 0],
                 [[3., .3], 0], [[3., 1.], 0], [[3., 3.], 0],)

if __name__ == '__main__':
    from itertools import product
    LOAD = True  # Set this to False to overwrite previous saved reconstructions
    PROBLEM = 3, 2
    # VERSION = 'general', 'noise', 'compare'
    # VERSION = 'generalUB', 'compareUB'
    VERSION = 'general', 'noise', 'compare', 'generalUB', 'compareUB', 'repeats', 'repeatsUB'
    ALGS = 'Random', 'Uniform', 'Bredies'

    def doPrint(problem, alg, p, cback):
        if not hasattr(cback, 'E'):
            print(f'Finished problem={problem}, algorithm={alg}, params={p}\n')
        else:
            j = list(cback.extras()).index('E')
            print(f'Final energy for problem={problem}, algorithm={alg}, params={p} is {cback.E[:, j].min()}\n')

    for problem, version in product(PROBLEM, VERSION):
        print(f'Starting problem={problem}, run={version}\n')
        filename = 'results2', 'problem_%d' % problem, version

        if version == 'general':  # compare 3 methods on the baseline, noise free problem
            EX = example(problem, noise=0, balanced=True)

            for alg in ALGS:
                F, D = alg_configs[alg]
                for p in D['single']:
                    recon, cback = F(*EX, problem, filename, p, LOAD=LOAD)
                    doPrint(problem, alg, p, cback)

        elif version == 'compare':  # compare algorithms with all different parameters
            EX = example(problem, noise=0, balanced=True)
            for alg in ALGS:
                F, D = alg_configs[alg]
                for p in D['all']:
                    recon, cback = F(*EX, problem, filename, p, LOAD=LOAD)
                    doPrint(problem, alg, p, cback)

        elif version == 'noise':  # compare performance vs Bredies on different noise levels
            for alg in ALGS:
                F, D = alg_configs[alg]

                if problem == 2 and alg == 'Bredies':
                    assert noise_configs == ([[1, 1], 0], [[1, 1], .2], [[1, 1], .6], [[3, 3], .6])

                for i, (scale, noise) in enumerate(noise_configs):
                    EX = example(problem, noise=noise, balanced=True)
                    EX[1].scale_kernel(scale)
                    record = filename + ('params_%d' % i,)

                    for p in D['single']:
                        if problem == 2 and alg == 'Bredies':
                            if i < 2:
                                recon, cback = F(*EX, problem, record, (p, noise), LOAD=LOAD)
                            elif i == 2:
                                recon, cback = F(*EX, problem, record, (p, noise), LOAD=LOAD)
                                recon, cback = F(*EX, problem, record, (p, noise, 'accelerated'), LOAD=LOAD)
                            elif i == 3:
                                recon, cback = F(*EX, problem, record, (p, noise, 'reg'), LOAD=LOAD)
                        else:
                            recon, cback = F(*EX, problem, record, p, LOAD=LOAD)
                        doPrint(problem, alg, (p, i), cback)

        elif version == 'scale':  # compare minimisers for different regularisation weights
            for alg in ('Uniform',):
                F, D = alg_configs[alg]
                for i, (scale, noise) in enumerate(scale_configs):
                    EX = example(problem, noise=noise, balanced=True)
                    EX[1].scale_kernel(scale)
                    record = filename + ('params_%d' % i,)
                    for p in D['single']:
                        recon, cback = F(*EX, problem, record, p, LOAD=LOAD)
                        doPrint(problem, alg, (p, i), cback)

        elif version == 'generalUB':  # compare 2 methods on the baseline, noise free problem
            EX = example(problem, noise=0, balanced=False)

            for alg in [alg + 'UB' for alg in ALGS if (alg + 'UB' in alg_configs)]:
                F, D = alg_configs[alg]
                for p in D['single']:
                    recon, cback = F(*EX, problem, filename, p, LOAD=LOAD)
                    doPrint(problem, alg, p, cback)

        elif version == 'compareUB':  # compare algorithms with all different parameters
            EX = example(problem, noise=0, balanced=False)
            for alg in [alg + 'UB' for alg in ALGS if (alg + 'UB' in alg_configs)]:
                F, D = alg_configs[alg]
                for p in D['all']:
                    recon, cback = F(*EX, problem, filename, p, LOAD=LOAD)
                    doPrint(problem, alg, p, cback)

        elif version == 'repeats':  # compare random repeats of the same alg
            EX = example(problem, noise=0, balanced=True)

            for alg in [alg + 'Rep' for alg in ALGS if (alg + 'Rep' in alg_configs)]:
                F, D = alg_configs[alg]
                for p in D['multiple']:
                    F(*EX, 100, problem, filename, p, LOAD=LOAD)
                    doPrint(problem, alg, p, None)

        elif version == 'repeatsUB':  # compare random repeats of the same unbalaced alg
            EX = example(problem, noise=0, balanced=False)

            for alg in [alg + 'UBRep' for alg in ALGS if (alg + 'UBRep' in alg_configs)]:
                F, D = alg_configs[alg]
                for p in D['multiple']:
                    F(*EX, 100, problem, filename, p, LOAD=LOAD)
                    doPrint(problem, alg, p, None)

        print(f'Finished problem={problem}, run={version}\n' + '-' * 50 + '\n')
