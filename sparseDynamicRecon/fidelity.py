'''
Created on 14 Oct 2021

@author: Rob
'''
import numpy as np
from .fidelityBin.utils import Fidelity, QuadraticFidelity
from .fidelityBin.WindowedFourier import WindowedFourierFidelity as WindowedFourier
from .fidelityBin.Fourier import FourierFidelity as Fourier
from .fidelityBin.Gaussian import GaussianFidelity as Gaussian
from .fidelityBin.AiryDisc import AiryDiscFidelity as AiryDisc

if __name__ == '__main__':
    from .dynamicModelsBin import DynamicMeasureSpace, CurveSpace
    np.random.seed(100)
    dim, T, epsilon, tau, datasz = 2, 11, 2, 1, 100
    padstr = lambda s, n: ' ' * ((n - len(s)) // 2) + s + ' ' * (n - len(s) - (n - len(s)) // 2)

    TEST = [1, 2, 3, 4, 5, 6]

    for test in TEST:
        for balanced in (True, False):
            FS = DynamicMeasureSpace(CurveSpace(dim=2, T=np.linspace(0, 1, T), balanced=balanced), n=10)

            rho = FS.rand()
            drho = FS.rand()
            if test == 1:
                name = 'Gaussian'
                F = Gaussian(np.random.rand(datasz, 2), 4, 1, np.random.rand(T, datasz))
            elif test == 2:
                name = 'Gaussian vec'
                F = Gaussian(np.random.rand(T, datasz, 2), 4, 1, np.random.rand(T, datasz))
            elif test == 3:
                name = 'Fourier'
                F = Fourier(np.random.rand(datasz, 2), np.random.rand(T, datasz))
                F = WindowedFourier(np.random.rand(datasz, 2), np.random.rand(T, datasz))
            elif test == 4:
                name = 'Fourier vec'
                F = Fourier(np.random.rand(T, datasz, 2), np.random.rand(T, datasz))
                F = WindowedFourier(np.random.rand(T, datasz, 2), np.random.rand(T, datasz))
            elif test == 5:
                name = 'AiryDisc'
                F = AiryDisc(np.random.rand(datasz, 2), 4, 1, np.random.rand(T, datasz))
            elif test == 6:
                name = 'AiryDisc vec'
                F = AiryDisc(np.random.rand(T, datasz, 2), 4, 1, np.random.rand(T, datasz))

            print('-' * 50, '\n%s Fidelity test' % name, '' if balanced else 'unbalanced')
            E0, G0 = F.grad(rho, discrete=True)
            E0, G0 = E0, (drho.arr * G0).sum()

            if abs(F(rho.arr + 1e-9 * drho.arr) - E0 - 1e-9 * G0) * 1e18 < 1e4:
                print('checked')
            else:
                print(''.join(padstr(s, 13) for s in ('E0 = E(x)', 'Et = E(x+ty)', 'err0=Et-E0', 'err0/t', 'err1=err0-tdE', 'err1/t^2')))
                for i in range(0, 10):
                    t = 10 ** (-i)
                    E1 = F(rho.arr + t * drho.arr)
                    print(''.join(padstr('% .3e' % f, 13) for f in (E0, E1, E1 - E0, (E1 - E0) / t, (E1 - E0 - t * G0),
                                            (E1 - E0 - t * G0) / t ** 2)))

            print('-' * 50, '\n%s Linearisation test' % name, '' if balanced else 'unbalanced')
            F = F.linearise(rho)
            E0, G0 = F.grad(rho)
            E0, G0 = E0, (drho.arr * G0).sum()
            if abs(F(rho.arr + 1e-9 * drho.arr) - E0 - 1e-9 * G0) * 1e18 < 1e4:
                print('checked')
            else:
                print(''.join(padstr(s, 13) for s in ('E0 = E(x)', 'Et = E(x+ty)', 'err0=Et-E0', 'err0/t', 'err1=err0-tdE', 'err1/t^2')))
                for i in range(0, 10):
                    t = 10 ** (-i)
                    E1 = F(rho.arr + t * drho.arr)
                    print(''.join(padstr('% .3e' % f, 13) for f in (E0, E1, E1 - E0, (E1 - E0) / t, (E1 - E0 - t * G0),
                                            (E1 - E0 - t * G0) / t ** 2)))

            print()
